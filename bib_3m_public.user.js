// ==UserScript==
// @name        BIB :: 3M (Public)
// @description Bibliotik upload button on 3M.
// @namespace   https://bibliotik.me/
// @version     1.0.0
// @author      mestreona
// @run-at      document-start
// @match       https://ebook.yourcloudlibrary.com/library/*
// @match       https://bibliotik.me/upload/ebooks*
// @connect     service.yourcloudlibrary.com
// @connect     bibliotik.me
// @require     https://unpkg.com/turndown@5.0.3/dist/turndown.js
// @grant       GM.setValue
// @grant       GM.getValue
// @grant       GM.deleteValue
// @grant       GM.xmlHttpRequest
// @grant       unsafeWindow
// ==/UserScript==

(function () {
    'use strict';

    class Router {
        constructor() {
            this.windowHost = window.location.host;
            this.windowPathname = window.location.pathname;
            this.windowLocation = window.location.toString();
            this.promises = [];
        }
        onHost(host, hostCallback) {
            if (this.windowHost == host || this.windowHost.endsWith(`.${host}`)) {
                this.matchLocation(hostCallback);
            }
            return this;
        }
        onHostRegex(host, hostCallback) {
            console.log(host, this.windowHost, host.test(this.windowHost));
            if (host.test(this.windowHost))
                this.matchLocation(hostCallback);
            return this;
        }
        on(location, cb) {
            if (this.windowLocation.indexOf(location) >= 0) {
                this.promises.push(new Promise(() => cb()));
            }
            return this;
        }
        run() {
            Promise.all(this.promises);
        }
        matchLocation(hostCallback) {
            const f = (path, cb) => {
                if (this.windowPathname.match(path)) {
                    this.promises.push(cb.mount());
                }
                return f;
            };
            hostCallback(f);
        }
    }

    const htmlToMarkdown = (html) => {
        const svc = new TurndownService();
        return svc.turndown(html).
            replace(/[ \t]+$/gm, "").
            replace(/^[*]\s+/gm, '* ').
            replace(/^[*] ([^\n]+)\n{2,}[*] /gm, "* $1\n* ").
            replace(/^[*] ([^\n]+)\n{2,}[*] /gm, "* $1\n* ").
            replace(/\n{3,}/g, "\n\n").
            toString();
    };
    const dispatchEvent = (element, event = "change", bubbles = true) => {
        element.dispatchEvent(new Event(event, { bubbles }));
    };
    const setInputValue = (selector, value) => {
        if (!value)
            return;
        const element = document.querySelector(selector);
        if (element) {
            element.value = value;
            dispatchEvent(element);
        }
    };
    const setSelectValue = (selector, value) => {
        if (!value)
            return;
        const element = document.querySelector(selector);
        if (element) {
            const options = new Map();
            element.querySelectorAll("option").forEach(el => {
                const text = el.innerText.trim();
                options.set(text, el.value || text);
            });
            element.value = options.get(value) || value;
            dispatchEvent(element);
        }
    };
    const setCheckboxValue = (selector, checked) => {
        if (checked === undefined || checked === null)
            return;
        const element = document.querySelector(selector);
        if (element) {
            element.checked = checked;
            dispatchEvent(element);
        }
    };
    const addClickHandler = (element, handler) => {
        element.addEventListener("click", async (event) => {
            event.preventDefault();
            await handler();
        });
    };
    const virtualDOM = document.implementation.createHTMLDocument('virtual');

    const observe = (node, f) => {
        const observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                mutation.addedNodes.forEach((value, _key, _parent) => {
                    if (value.nodeType == 1)
                        f(value);
                });
            });
        });
        observer.observe(node, { childList: true, subtree: true });
    };
    const observeById = (id, f) => {
        const container = document.getElementById(id);
        if (container)
            observe(container, f);
    };

    const angularDebug = () => {
        unsafeWindow.name = 'NG_ENABLE_DEBUG_INFO!' + unsafeWindow.name;
    };

    const authorName = (name) => {
        return name.
            replace(", MD", "").
            replace(" USA Inc.", "").
            replace(" DDS", "").
            replace(" MBA", "").
            replace(" ODD", "").
            replace(/\bDr\.? /, "").
            replace(/,? Ph\.?D\.?/i, "").
            replace(/,? M\.?D\.?/, "").
            toString();
    };

    class BookPage {
        get format() { return ""; }
        get language() { return ""; }
        get supplements() { return false; }
        get retail() { return true; }
        get anonymous() { return true; }
        get notify() { return true; }
        createBIBBookOnClick(button) {
            addClickHandler(button, () => this.createBIBBook());
        }
        async createBIBBook() {
            const book = {
                title: this.title,
                authors: this.authors,
                editors: this.editors,
                contributors: this.contributors,
                translators: this.translators,
                publishers: this.publishers,
                isbn: this.isbn,
                year: this.year,
                pages: this.pages,
                supplements: this.supplements,
                format: this.format,
                language: this.language,
                retail: this.retail,
                tags: this.tags,
                image: this.image,
                description: this.description,
                anonymous: this.anonymous,
                notify: this.notify,
            };
            await GM.setValue("bibBook", JSON.stringify(book));
            window.open("https://bibliotik.me/upload/ebooks");
        }
    }

    const fillBookForm = (book) => {
        if (!book)
            return;
        setInputValue("#TitleField", book.title);
        setInputValue("#AuthorsField", book.authors);
        setInputValue("#EditorsField", book.editors);
        setInputValue("#ContributorsField", book.contributors);
        setInputValue("#TranslatorsField", book.translators);
        setInputValue("#PublishersField", book.publishers);
        setInputValue("#IsbnField", book.isbn);
        setInputValue("#YearField", book.year);
        setInputValue("#PagesField", book.pages);
        setCheckboxValue("#SupplementsField", book.supplements);
        setSelectValue("#FormatField", book.format);
        setSelectValue("#LanguageField", book.language);
        setCheckboxValue("#RetailField", book.retail);
        setInputValue("#TagsField", book.tags);
        setInputValue("#ImageField", book.image);
        setInputValue("#DescriptionField", book.description);
        setCheckboxValue("#AnonymousField", book.anonymous);
        setCheckboxValue("#NotifyField", book.notify);
        if (book.contributors || book.translators) {
            const options = document.getElementById("creatorOptions");
            if (options)
                options.style.display = "";
        }
        if (!book.format) {
            const formatField = document.getElementById("FormatField");
            if (formatField) {
                formatField.scrollIntoView();
                formatField.focus();
            }
        }
    };

    class CloudLibraryBookPage extends BookPage {
        constructor(book) {
            super();
            this.book = book;
        }
        static async mount() {
            observeById("modals", (node) => this.addUploadButton(node));
        }
        static addUploadButton(node) {
            var _a;
            if (!((_a = node.classList) === null || _a === void 0 ? void 0 : _a.contains("ng-scope")))
                return;
            if (!(node.getAttribute("ng-if") === "!working"))
                return;
            const scope = angular.element(node).scope();
            const book = scope.$parent.item;
            if (!book.Title)
                return;
            const div = document.createElement("div");
            div.id = "bib-toolbar";
            const uploadButton = this.createButton("» BIB", "blue");
            div.appendChild(uploadButton);
            const page = new CloudLibraryBookPage(book);
            page.createBIBBookOnClick(uploadButton);
            if (book.ReaktorPatronAction == "LOAN") {
                const downloadButton = this.createButton("ACSM", "green");
                addClickHandler(downloadButton, () => page.downloadACSM());
                div.appendChild(downloadButton);
            }
            node.insertAdjacentElement("afterbegin", div);
        }
        static createButton(caption, color) {
            const button = document.createElement("button");
            button.className = `button-${color}`;
            button.innerText = caption;
            button.style.display = "block";
            button.style.marginBottom = "10px";
            return button;
        }
        async downloadACSM() {
            const libUrl = unsafeWindow.mmmWebPatron.UserSession.Library.UrlName;
            await GM.xmlHttpRequest({
                method: "GET",
                url: `https://ebook.yourcloudlibrary.com/uisvc/${libUrl}/Patron/Borrowed`,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                },
                onload: (response) => {
                    const borrow = JSON.parse(response.responseText).find((el) => el.Id == this.book.Id);
                    if (!borrow) {
                        alert(`Can't find a borrowed book with ID ${this.book.Id}`);
                        return;
                    }
                    const borrowId = this.decodeToken(borrow.Obii);
                    const url = `https://service.yourcloudlibrary.com/delivery/metadata?exporter=com.bookpac.exporter.fulfillmenttoken&token=${this.token}&tokenType=vendorID&udid=${borrowId}`;
                    window.open(url);
                },
            });
        }
        decodeToken(token) {
            return atob(token.replace(/^x-(.*)$/, "$1"));
        }
        get token() {
            const token = unsafeWindow.mmmWebPatron.UserSession.Osi;
            if (!token)
                return null;
            return this.decodeToken(token);
        }
        get title() {
            var _a, _b;
            const subtitle = (_a = this.book.SubTitle) !== null && _a !== void 0 ? _a : "";
            let title = (_b = this.book.Title) !== null && _b !== void 0 ? _b : "";
            if (subtitle != "") {
                title = `${title}: ${subtitle}`;
            }
            title = title
                .replace(/,?\s*First Edition/i, "")
                .replace(/,?\s*Second Edition/i, " (2nd Edition)")
                .replace(/,?\s*Third Edition/i, " (3rd Edition)")
                .replace(/,?\s*Fourth Edition/i, " (4th Edition)")
                .replace(/,?\s*Fifth Edition/i, " (5th Edition)");
            return title;
        }
        get authors() {
            var _a;
            let authors = (_a = this.book.Authors) !== null && _a !== void 0 ? _a : "";
            authors = authors
                .split(/\s*;\s*/)
                .map((a) => a
                .split(/\s*,\s*/, 2)
                .reverse()
                .join(" "))
                .map((a) => authorName(a))
                .join(", ");
            return authors;
        }
        get editors() {
            return "";
        }
        get contributors() {
            return "";
        }
        get translators() {
            return "";
        }
        get publishers() {
            if (!this.book.Publisher)
                return "";
            let publisher = this.book.Publisher.split("/", 1)[0];
            if (!publisher)
                return "";
            publisher = publisher.replace(", LLC", "");
            return publisher.trim();
        }
        get format() {
            return this.book.Format == "EPUB3" ? "EPUB" : this.book.Format;
        }
        get isbn() {
            var _a;
            return (_a = this.book.ISBN) !== null && _a !== void 0 ? _a : "";
        }
        get year() {
            return this.book.PublicationYear.toString();
        }
        get image() {
            return (this.book.ImagePath.replace("size=NORMAL", "size=LARGE") + "#.jpg");
        }
        get pages() {
            return "";
        }
        get tags() {
            return "";
        }
        get description() {
            return htmlToMarkdown(this.book.Description)
                .replace(/\\+"/g, '"')
                .toString();
        }
    }

    class BibliotikUploadPage {
        static async mount() {
            const bookString = await GM.getValue("bibBook");
            if (typeof bookString == "string") {
                const book = JSON.parse(bookString);
                fillBookForm(book);
                await GM.deleteValue("bibBook");
            }
        }
    }

    if (window.location.hostname === "ebook.yourcloudlibrary.com")
        angularDebug();
    window.addEventListener("DOMContentLoaded", () => {
        new Router()
            .onHost("ebook.yourcloudlibrary.com", (match) => {
            match("^/library", CloudLibraryBookPage);
        })
            .onHost("bibliotik.me", (match) => {
            match("^/upload", BibliotikUploadPage);
        })
            .run();
    });

}());
